This project contains feature to look up Nikon D3X product on Amazon's website 

Cypress tests for amazon.com with following details:

- Search Nikon and sort results from highest price to lowest.
- Select second product and click it for details.
- From details check (verify with assert) that product topic contains text “Nikon D3X”

Prerequisites:

- Node.js installed.

How to run test:

- Open CMD and navigate to cloned repo directory/node_modules
- Enter command 'npx cypress open'. Cypress runner opens in a few seconds.
- Click on the .feature file to start test run.